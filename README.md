HCPL-0872
The Digital Interface IC, HCPL-0872 converts the singlebit
data stream from the Isolated Modulator (such as
HCPL-7860/786J/7560) into fifteen-bit output words and
provides a serial output interface that is compatible with
SPI®, QSPI®, and Microwire® protocols, allowing direct
connection to a microcontroller.


HCPL.vhd is just modeling of this module.